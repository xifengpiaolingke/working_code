﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace MeetingService.ModbusService
{
   public class ModbusServiceMain
    {
        // 设置定时器
        private System.Timers.Timer t = null;

        public void Init()
        {
            try
            {
                if (t == null)
                {
                    t = new System.Timers.Timer();
                    t.Elapsed += new ElapsedEventHandler(ReadData);             //读取数据
                    t.Elapsed += new ElapsedEventHandler(ReadCoil);             //读取线圈
                    t.Elapsed += new ElapsedEventHandler(ReadDiscrete);         //读取离散数据
                    t.Elapsed += new ElapsedEventHandler(Read);                 //读取寄存器
                    t.Elapsed += new ElapsedEventHandler(WriteData);            //写入数据
                    t.Elapsed += new ElapsedEventHandler(WriteCoil);            //写入单个线圈
                    t.Elapsed += new ElapsedEventHandler(WriteOneRegister);     //写入单个寄存器
                    t.Elapsed += new ElapsedEventHandler(WriteCoils);           //写入多个线圈
                    t.Elapsed += new ElapsedEventHandler(WriteDatas);           //写入多个寄存器

                    t.Interval = 5000;      // 指定执行的间隔时间
                    t.Enabled = true;       // 是否启用执行 System.Timers.Timer.Elapsed 事件
                    t.AutoReset = true;     // 设置为 true 表示一直执行，false 为只执行一次
                }
            }
            catch
            {
                t.Stop();
                t.Dispose();
            }
        }

        /// <summary>
        /// 读取数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public void ReadData(object sender, ElapsedEventArgs args)
        {
            try
            {

                ((System.Timers.Timer)sender).Enabled = false;  //单线程管控
                Modbus modbus = new Modbus();
                modbus.ReadData();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ((System.Timers.Timer)sender).Enabled = true;  //单线程管控
            }
        }

        /// <summary>
        /// 读取线圈
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public void ReadCoil(object sender, ElapsedEventArgs args)
        {
            try
            {
                ((System.Timers.Timer)sender).Enabled = false;  //单线程管控
                Modbus modbus = new Modbus();
                modbus.ReadCoil();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ((System.Timers.Timer)sender).Enabled = true;  //单线程管控
            }
        }

        /// <summary>
        /// 读取离散数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public void ReadDiscrete(object sender, ElapsedEventArgs args)
        {
            try
            {
                ((System.Timers.Timer)sender).Enabled = false;  //单线程管控
                Modbus modbus = new Modbus();
                modbus.ReadDiscrete();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ((System.Timers.Timer)sender).Enabled = true;  //单线程管控
            }
        }

        /// <summary>
        /// 读取寄存器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public void Read(object sender, ElapsedEventArgs args)
        {
            try
            {
                ((System.Timers.Timer)sender).Enabled = false;  //单线程管控
                Modbus modbus = new Modbus();
                modbus.Read();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ((System.Timers.Timer)sender).Enabled = true;  //单线程管控
            }
        }

        /// <summary>
        /// 写入数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public void WriteData(object sender, ElapsedEventArgs args)
        {
            try
            {
                ((System.Timers.Timer)sender).Enabled = false;  //单线程管控
                Modbus modbus = new Modbus();
                modbus.WriteData();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ((System.Timers.Timer)sender).Enabled = true;  //单线程管控
            }
        }

        /// <summary>
        /// 写入单个线圈
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public void WriteCoil(object sender, ElapsedEventArgs args)
        {
            try
            {
                ((System.Timers.Timer)sender).Enabled = false;  //单线程管控
                Modbus modbus = new Modbus();
                modbus.WriteCoil();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ((System.Timers.Timer)sender).Enabled = true;  //单线程管控
            }
        }

        /// <summary>
        /// 写入单个寄存器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public void WriteOneRegister(object sender, ElapsedEventArgs args)
        {
            try
            {
                ((System.Timers.Timer)sender).Enabled = false;  //单线程管控
                Modbus modbus = new Modbus();
                modbus.WriteOneRegister();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ((System.Timers.Timer)sender).Enabled = true;  //单线程管控
            }
        }

        /// <summary>
        /// 批量写入线圈
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public void WriteCoils(object sender, ElapsedEventArgs args)
        {
            try
            {
                ((System.Timers.Timer)sender).Enabled = false;  //单线程管控
                Modbus modbus = new Modbus();
                modbus.WriteCoils();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ((System.Timers.Timer)sender).Enabled = true;  //单线程管控
            }
        }

        /// <summary>
        /// 批量写入寄存器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public void WriteDatas(object sender, ElapsedEventArgs args)
        {
            try
            {
                ((System.Timers.Timer)sender).Enabled = false;  //单线程管控
                Modbus modbus = new Modbus();
                modbus.WriteDatas();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ((System.Timers.Timer)sender).Enabled = true;  //单线程管控
            }
        }
    }
}
