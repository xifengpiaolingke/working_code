﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HslCommunication;
using HslCommunication.ModBus;

namespace MeetingService.ModbusService
{
  public class Modbus
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("mettingInfo");
        private static readonly log4net.ILog errorLog = log4net.LogManager.GetLogger("mettingError");

        public ModbusTcpNet busTcpClient = new ModbusTcpNet("192.168.1.195", 502, 0x01);   // 站号1

        //public ModbusRtu busRtuClient = new ModbusRtu(0x01);

        //在Modbus服务器的设备里，大部分的设备都是从地址0开始的，有些特殊的设备是从地址1开始的，所以本组件里面，默认从地址0开始，如果想要从地址1开始，那么就需要如下的配置：
        //busTcpClient.AddressStartWithZero = False;

        //busTcpClient.ConnectServer();//连接服务
        //busTcpClient.ConnectClose( );//关闭连接

        #region 读取数据

        /// <summary>
        /// 读取数据
        /// 演示常用的读写操作，为了方便起见，不再对IsSuccess判断，一般都是成功的
        /// </summary>
        public void ReadData()
        {
            log.Info("不对IsSuccess判断，读取数据开始");
            // 读取操作
            bool coil100 = busTcpClient.ReadCoil("100").Content;   // 读取线圈100的通断
            short short100 = busTcpClient.ReadInt16("100").Content; // 读取寄存器100的short值
            ushort ushort100 = busTcpClient.ReadUInt16("100").Content; // 读取寄存器100的ushort值
            int int100 = busTcpClient.ReadInt32("100").Content;      // 读取寄存器100-101的int值
            uint uint100 = busTcpClient.ReadUInt32("100").Content;   // 读取寄存器100-101的uint值
            float float100 = busTcpClient.ReadFloat("100").Content; // 读取寄存器100-101的float值
            long long100 = busTcpClient.ReadInt64("100").Content;    // 读取寄存器100-103的long值
            ulong ulong100 = busTcpClient.ReadUInt64("100").Content; // 读取寄存器100-103的ulong值
            double double100 = busTcpClient.ReadDouble("100").Content; // 读取寄存器100-103的double值
            string str100 = busTcpClient.ReadString("100", 5).Content;// 读取100到104共10个字符的字符串
            log.Info("不对IsSuccess判断，读取数据结束");
        }

        /// <summary>
        /// 读取线圈API：
        /// 在此处举例读取地址为0，长度为10的线圈数量，读取出来的数据已经自动转化成了bool数组，方便的进行二次处理：
        /// </summary>
        public void ReadCoil()
        {
            HslCommunication.OperateResult<bool[]> read = busTcpClient.ReadCoil("0", 10);
            if (read.IsSuccess)
            {
                bool coil_0 = read.Content[0];
                bool coil_1 = read.Content[1];
                bool coil_2 = read.Content[2];
                bool coil_3 = read.Content[3];
                bool coil_4 = read.Content[4];
                bool coil_5 = read.Content[5];
                bool coil_6 = read.Content[6];
                bool coil_7 = read.Content[7];
                bool coil_8 = read.Content[8];
                bool coil_9 = read.Content[9];
                log.Info("读取线圈API成功");
            }
            else
            {
                errorLog.Error(read.ToMessageShowString());
            }
        }

        /// <summary>
        /// 读取离散数据：
        /// 读取离散数据和读取线圈的代码几乎是一致的，处理方式也是一致的，只是方法名称改了
        /// </summary>
        public void ReadDiscrete()
        {
            HslCommunication.OperateResult<bool[]> read = busTcpClient.ReadDiscrete("0", 10);
            if (read.IsSuccess)
            {
                bool coil_0 = read.Content[0];
                bool coil_1 = read.Content[1];
                bool coil_2 = read.Content[2];
                bool coil_3 = read.Content[3];
                bool coil_4 = read.Content[4];
                bool coil_5 = read.Content[5];
                bool coil_6 = read.Content[6];
                bool coil_7 = read.Content[7];
                bool coil_8 = read.Content[8];
                bool coil_9 = read.Content[9];
                log.Info("读取离散数据成功");
            }
            else
            {
                errorLog.Error(read.ToMessageShowString());
            }
        }

        /// <summary>
        /// 读取寄存器数据：
        /// 假设我们需要读取地址为0，长度为10的数据，也即是10个数据，每个数据2个字节，总计20个字节的数据。下面解析数据前，先进行了假设，你在解析自己的数据前可以参照下面的解析
        /// </summary>
        public void Read()
        {
            HslCommunication.OperateResult<byte[]> read = busTcpClient.Read("0", 10);
            if (read.IsSuccess)
            {
                // 共返回20个字节，每个数据2个字节，高位在前，低位在后
                // 在数据解析前需要知道里面到底存了什么类型的数据，所以需要进行一些假设：
                // 前两个字节是short数据类型
                short value1 = busTcpClient.ByteTransform.TransInt16(read.Content, 0);
                // 接下来的2个字节是ushort类型
                ushort value2 = busTcpClient.ByteTransform.TransUInt16(read.Content, 2);
                // 接下来的4个字节是int类型
                int value3 = busTcpClient.ByteTransform.TransInt32(read.Content, 4);
                // 接下来的4个字节是float类型
                float value4 = busTcpClient.ByteTransform.TransSingle(read.Content, 8);
                // 接下来的全部字节，共8个字节是规格信息
                string speci = Encoding.ASCII.GetString(read.Content, 12, 8);
                // 已经提取完所有的数据
                log.Info("读取寄存器数据成功");
            }
            else
            {
                errorLog.Error(read.ToMessageShowString());
            }
        }
        #endregion

        #region 写入数据

        /// <summary>
        /// 写入数据
        /// 演示常用的读写操作，为了方便起见，不再对IsSuccess判断，一般都是成功的
        /// </summary>
        public void WriteData()
        {
            log.Info("不对IsSuccess判断，写入数据开始");
            // 写入操作
            busTcpClient.Write("100", true);// 写入线圈100为通
            busTcpClient.Write("100", (short)12345);// 写入寄存器100为12345
            busTcpClient.Write("100", (ushort)45678);// 写入寄存器100为45678
            busTcpClient.Write("100", 123456789);// 写入寄存器100-101为123456789
            busTcpClient.Write("100", (uint)123456778);// 写入寄存器100-101为123456778
            busTcpClient.Write("100", 123.456);// 写入寄存器100-101为123.456
            busTcpClient.Write("100", 12312312312414L);//写入寄存器100-103为一个大数据
            busTcpClient.Write("100", 12634534534543656UL);// 写入寄存器100-103为一个大数据
            busTcpClient.Write("100", 123.456d);// 写入寄存器100-103为一个双精度的数据
            busTcpClient.Write("100", "K123456789");
            log.Info("不对IsSuccess判断，写入数据结束");
        }

        /// <summary>
        /// 写入单个线圈：
        /// 一个线圈，这个相对比较简单，假设我们需要写入线圈0，为通
        /// </summary>
        public void WriteCoil()
        {
            HslCommunication.OperateResult write = busTcpClient.Write("0", true);
            if (write.IsSuccess)
            {
                // 写入成功
               log.Info("写入单个线圈成功");
            }
            else
            {
                errorLog.Error(write.ToMessageShowString());
            }
        }

        /// <summary>
        /// 写入单个寄存器
        /// 写一个寄存器的操作也是非常的方便，在这里提供了三个重载的方法，允许使用三种方式写入：分别写入，short，ushort，byte三种：
        /// </summary>
        public void WriteOneRegister()
        {
            short value = -1234;
            //ushort value = 56713;
            HslCommunication.OperateResult write = busTcpClient.Write("0", value);

            // 0x00为高位，0x10为低位
            //HslCommunication.OperateResult write = busTcpClient.WriteOneRegister("0", 0x00, 0x10);
            if (write.IsSuccess)
            {
                // 写入成功
                log.Info("写入单个寄存器成功");
            }
            else
            {
                errorLog.Error(write.ToMessageShowString());
            }
        }

        /// <summary>
        /// 批量写入线圈：
        /// </summary>
        public void WriteCoils()
        {
            // 线圈0为True，线圈1为false，线圈2为true.....等等，以此类推，数组长度多少，就写入多少线圈
            bool[] value = new bool[] { true, false, true, true, false, false };
            HslCommunication.OperateResult write = busTcpClient.Write("0", value);
            if (write.IsSuccess)
            {
                // 写入成功
                log.Info("批量写入线圈成功");
            }
            else
            {
                errorLog.Error(write.ToMessageShowString());
            }
        }

        /// <summary>
        /// 批量写入寄存器：
        /// 第一种情况写入一串short数组，这种情况比较简单：
        /// 第二情况写入一串ushort数组，也是比较简单：
        /// 比较复杂的是写入自定义的数据，按照上述读取寄存器，比如我需要写入寄存器0，寄存器1共同组成的一个int数据，那么我们这么写：
        /// </summary>
        public void WriteDatas()
        {
            short[] value = new short[] { -1234, 467, 12345 };
            HslCommunication.OperateResult write = busTcpClient.Write("0", value);

            //ushort[] value = new ushort[] { 46789, 467, 12345 };
            //HslCommunication.OperateResult write = busTcpClient.Write("0", value);

            //int value = 12345678;// 等待写入的一个数据
            //HslCommunication.OperateResult write = busTcpClient.Write("0", value);
            if (write.IsSuccess)
            {
                // 写入成功
                log.Info("批量写入寄存器成功");
            }
            else
            {
                errorLog.Error(write.ToMessageShowString());
            }
        }


        #endregion

    }
}
