﻿using MeetingService.ModbusService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace MeetingService
{
    public partial class MeetingService : ServiceBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("mettingInfo");
        public MeetingService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            ModbusServiceMain sm = new ModbusServiceMain();
            string start = string.Format("{0}-{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), "程序启动了。");
            log.Info(start);
            sm.Init();


        }

        protected override void OnStop()
        {
            string stop = string.Format("{0}-{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), "程序停止了。");
            log.Info(stop);
        }

        // 计算机关闭执行方法
        protected override void OnShutdown()
        {
            string shutdown = string.Format("{0}-{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), "电脑关闭了。");
            log.Info(shutdown);
        }

        // 恢复服务执行方法
        protected override void OnContinue()
        {
            string continueValue = string.Format("{0}-{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), "服务恢复了。");
            log.Info(continueValue);
        }

        // 暂停服务执行方法
        protected override void OnPause()
        {
            string pause = string.Format("{0}-{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), "服务暂停了。");
            log.Info(pause);
        }

    }
}
