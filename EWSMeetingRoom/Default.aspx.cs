﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Exchange.WebServices;
using Microsoft.Exchange.WebServices.Data;
using Microsoft.Exchange.WebServices.Autodiscover;
using System.Configuration;
using System.IO;
using System.Text;
using System.Net;


namespace EWSMeetingRoom
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 获取用户在某段时间内的所有会议。最长时间间隔为42天。
        /// </summary>
        /// <param name="startDate">要查询的开始时间。</param>
        /// <param name="endDate">要查询的结束时间</param>
        /// <param name="mailbox">要查询的邮箱日历，如果查询当前登录的用户的日历，可不填。</param>
        /// <returns>返回一个会议列表。列表对象不包含详细信息（如接受拒绝状态等），需使用<see cref="GetMeetingStatus"/> 方法获取。</returns>
        public List<Appointment> GetMeetings(DateTime startDate, DateTime endDate, string mailbox = null)
        {
            ExchangeService Service = GetExchangeService();
            CalendarFolder calendar = null;

            if (mailbox == null)
            {
                calendar = CalendarFolder.Bind(Service, WellKnownFolderName.Calendar);
            }
            else
            {
                FolderId folderid = new FolderId(WellKnownFolderName.Calendar, mailbox);
                calendar = CalendarFolder.Bind(Service, folderid);
            }

            // Set the start and end time and number of appointments to retrieve.
            CalendarView cView = new CalendarView(startDate, endDate);

            // Limit the properties returned to the appointment's subject, start time, and end time.
            cView.PropertySet = new PropertySet(AppointmentSchema.Subject,
                AppointmentSchema.LastModifiedTime,
                AppointmentSchema.IsMeeting,
                AppointmentSchema.Start,
                AppointmentSchema.End);

            // Retrieve a collection of appointments by using the calendar view.
            FindItemsResults<Appointment> appointments = calendar.FindAppointments(cView);
            return appointments.Where(m => m.IsMeeting).ToList();
        }
        private ExchangeService GetExchangeService()
        {
            ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2007_SP1);
            //service.Credentials = new WebCredentials(@"ad001\z002drva", "4rfv%TGB");
            //service.Credentials = new WebCredentials(@"wang.lw@siemens.com", "");
            service.Credentials = new NetworkCredential(@"ad001\z002drva", "4rfv%TGB");
            //service.Credentials = new NetworkCredential(@"cn001\cn1bm011", "2wsx#EDC");

            // service.AutodiscoverUrl("bobsmith@yourdomain.onmicrosoft.com", RedirectionUrlValidationCallback);
       
            service.Url = new Uri("https://cnpek01905.cn001.siemens.net/EWS/Exchange.asmx");
            return service;
        }
      
        protected void Button1_Click(object sender, EventArgs e)
        {

            ExchangeService service = GetExchangeService();
            var folderView = new FolderView(100);   // or something like 100, idunno
            folderView.Traversal = FolderTraversal.Deep;
            folderView.PropertySet = new PropertySet(FolderSchema.FolderClass, FolderSchema.DisplayName, FolderSchema.TotalCount, FolderSchema.ParentFolderId);   // ... and/or whatever else you want to get - folderclass is important though. 
            //ServicePointManager.ServerCertificateValidationCallback = sender, certificate, chain, sslPolicyErrors) => true;
            FindFoldersResults folders = service.FindFolders(WellKnownFolderName.MsgFolderRoot, folderView);
            // Process each item.
            foreach (Folder myFolder in folders.Folders)
            {
                if (myFolder is CalendarFolder)
                {
                    var calendar = (myFolder as CalendarFolder);
                    // Initialize values for the start and end times, and the number of appointments to retrieve.
                    DateTime startDate = new DateTime(2017, 7, 10);
                    DateTime endDate = startDate.AddDays(3);
                    const int NUM_APPTS = 15;
                    // Set the start and end time and number of appointments to retrieve.
                    CalendarView cView = new CalendarView(startDate, endDate, NUM_APPTS);
                    // Limit the properties returned to the appointment's subject, start time, and end time.
                    cView.PropertySet = new PropertySet(AppointmentSchema.Subject, AppointmentSchema.Start, AppointmentSchema.End);
                    // Retrieve a collection of appointments by using the calendar view.
                    FindItemsResults<Appointment> appointments = calendar.FindAppointments(cView);
                    foreach (Appointment a in appointments)
                    {
                        Response.Write("Subject: " + a.Subject.ToString() + " ");                        
                        Response.Write("<br>");
                        //Console.Write("Subject: " + a.Subject.ToString() + " ");
                        //Console.Write("Start: " + a.Start.ToString() + " ");
                        //Console.Write("End: " + a.End.ToString());
                        //Console.WriteLine();
                    }
                }
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            try
            {
                //DateTime startDate = DateTime.Now;
                DateTime now = DateTime.Now;
                DateTime startDate = now.AddDays(-90);
                DateTime endDate = now.AddDays(3);
                List<Appointment> appointments = GetMeetings(startDate, endDate, "bsce_meetingroom_118.cn@siemens.com");
                foreach (Appointment a in appointments)
                {
                    if (a.Subject== null)
                        Response.Write("Subject: ");                        
                    else
                        Response.Write("Subject: " + a.Subject.ToString() + " ");
                    //Response.Write("Start: " + a.Start.ToString() + " ");
                    //Response.Write("End: " + a.End.ToString());
                    Response.Write("<br>");
                    //Console.Write("Subject: " + a.Subject.ToString() + " ");
                    //Console.Write("Start: " + a.Start.ToString() + " ");
                    //Console.Write("End: " + a.End.ToString());
                    //Console.WriteLine();
                }
            }
            catch (Exception ee)
            {
            }
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            DateTime startDate = DateTime.Now;
            DateTime endDate = startDate.AddDays(2);
             List<AppointmentProperty> ps = EWSHelper.GetAppointment(startDate, endDate);
             foreach (AppointmentProperty p in ps)
             {
                 Response.Write("Subject: " + p.Subject + " ");
                 Response.Write("<br>");
                 //Console.Write("Subject: " + a.Subject.ToString() + " ");
                 //Console.Write("Start: " + a.Start.ToString() + " ");
                 //Console.Write("End: " + a.End.ToString());
                 //Console.WriteLine();
             }
            
        }
    }
}
