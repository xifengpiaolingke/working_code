﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Exchange.WebServices.Data;
using Microsoft.Exchange.WebServices;
using System.Collections;
using System.IO;
using System.Net;

namespace EWSMeetingRoom
{
  /// <summary>
    /// Exchange操作类
    /// 需要引用：Microsoft.Exchange.WebServices.dll
    /// </summary>
    public class EWSHelper
    {
        /// <summary>
        /// ExchangeService对象
        /// </summary>
        private static ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2010);

        /// <summary>
        /// 初始化ExchangeService对象
        /// </summary>
        private static void InitializeEWS()
        {
            service.Credentials = new NetworkCredential(@"ad001\z003wkdd", "5tgb^YHN");
            service.Url = new Uri("https://mail-cn.siemens.net/EWS/exchange.asmx");
        }

        #region 会议相关操作
        /// <summary>
        /// 获取会议室列表
        /// </summary>
        /// <returns>会议室列表</returns>
        public static EmailAddressCollection GetRoomList()
        {
            InitializeEWS();
            EmailAddressCollection RoomList = service.GetRoomLists();
            return RoomList;
        }

        /// <summary>
        /// 保存会议
        /// </summary>
        /// <param name="ap">会议属性</param>
        /// <returns>保存结果</returns>
        public static bool CreateAppointment(AppointmentProperty ap)
        {
            try
            {
                InitializeEWS();
                //初始化会议对象
                Appointment appointment = new Appointment(service);
                //会议主题
                appointment.Subject = ap.Subject;
                //会议内容
                appointment.Body = ap.Body;
                //会议开始
                appointment.Start = ap.Start;
                //会议结束
                appointment.End = ap.End;
                //会议的位置
                appointment.Location = ap.Location;
                //添加与会者
                foreach (Attendee attendee in ap.Attendees)
                {
                    appointment.RequiredAttendees.Add(attendee);
                }
                //保存会议
                appointment.Save();

                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        /// <summary>
        /// 获取指定时间段的会议列表
        /// </summary>
        /// <param name="start">开始时间</param>
        /// <param name="end">结束时间</param>
        /// <returns>会议列表</returns>
        public static List<AppointmentProperty> GetAppointment(DateTime start, DateTime end)
        {
            InitializeEWS();
            //要返回的会议列表
            List<AppointmentProperty> appointments = new List<AppointmentProperty>();
            //要获取的时间段
            CalendarView cv = new CalendarView(start, end);
            FindItemsResults<Appointment> aps = service.FindAppointments(WellKnownFolderName.Calendar, cv);
            foreach (Appointment ap in aps)
            {
                //定义需要的会议属性
                PropertyDefinitionBase[] bas = new PropertyDefinitionBase[] { AppointmentSchema.Id, AppointmentSchema.Start, AppointmentSchema.End, AppointmentSchema.Subject, AppointmentSchema.Body, AppointmentSchema.RequiredAttendees, AppointmentSchema.Location };
                PropertySet props = new PropertySet(bas);
                Appointment email = Appointment.Bind(service, ap.Id, props);

                AppointmentProperty appointment = new AppointmentProperty();
                appointment.Start = email.Start;
                appointment.End = email.End;
                appointment.Body = email.Body;
                appointment.Subject = email.Subject;
                appointment.Location = email.Location;
                appointment.Attendees = email.RequiredAttendees;
                appointment.ID = email.Id;

                appointments.Add(appointment);
            }
            return appointments;
        }

        /// <summary>
        /// 导出会议到文件
        /// </summary>
        /// <param name="Path">目的物理路径</param>
        /// <param name="Count">要导出的数量</param>
        /// <returns>导出结果</returns>
        public static bool ExportMIMEAppointment(string Path, int Count)
        {
            try
            {
                InitializeEWS();
                Folder inbox = Folder.Bind(service, WellKnownFolderName.Calendar);
                ItemView view = new ItemView(Count);
                view.PropertySet = new PropertySet(BasePropertySet.IdOnly);
                FindItemsResults<Item> results = inbox.FindItems(view);
                foreach (var item in results)
                {
                    PropertyDefinitionBase[] bas = new PropertyDefinitionBase[] { AppointmentSchema.Id, AppointmentSchema.Start, AppointmentSchema.End, AppointmentSchema.Subject, AppointmentSchema.Body, AppointmentSchema.RequiredAttendees, AppointmentSchema.Location, AppointmentSchema.MimeContent };
                    PropertySet props = new PropertySet(bas);
                    Appointment email = Appointment.Bind(service, item.Id, props);
                    string iCalFileName = @Path + email.Start.ToString("yyyy-MM-dd") + "_" + email.Subject + ".ics";
                    using (FileStream fs = new FileStream(iCalFileName, FileMode.Create, FileAccess.Write))
                    {
                        fs.Write(email.MimeContent.Content, 0, email.MimeContent.Content.Length);
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        #endregion

        #region 邮件相关操作
        /// <summary>
        /// 获取邮件列表
        /// </summary>
        /// <param name="IsRead">已读true/未读false</param>
        /// <param name="Count">数量</param>
        /// <returns>邮件列表</returns>
        public static List<EmailMessage> GetEmailList(bool IsRead, int Count)
        {
            InitializeEWS();
            List<EmailMessage> emails = new List<EmailMessage>();
            //创建过滤器
            SearchFilter sf = new SearchFilter.IsEqualTo(EmailMessageSchema.IsRead, IsRead);
            FindItemsResults<Item> findResults = null;
            try
            {
                findResults = service.FindItems(WellKnownFolderName.Inbox, sf, new ItemView(Count));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            foreach (Item item in findResults.Items)
            {
                EmailMessage email = EmailMessage.Bind(service, item.Id);
                emails.Add(email);
            }
            return emails;
        }

        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="Subject">邮件标题</param>
        /// <param name="Body">邮件正文</param>
        /// <param name="emails">收件人列表</param>
        /// <returns>发送结果</returns>
        public static bool SendEmail(string Subject, string Body, List<EmailAddress> emails)
        {
            try
            {
                InitializeEWS();
                EmailMessage message = new EmailMessage(service);
                // 邮件主题
                message.Subject = Subject;
                message.Body = new MessageBody();
                // 指定发送邮件的格式，可以是Text和Html格式
                message.Body.BodyType = BodyType.Text;
                // 邮件内容
                message.Body.Text = Body;
                // 可以添加多个邮件人.也可以添加一个集合，用
                foreach (EmailAddress email in emails)
                {
                    message.ToRecipients.Add(email);
                }
                // 保存草稿
                //message.save();
                // 只发送不保存邮件
                // message.Send();
                // 发送并保存已发送邮件
                message.SendAndSaveCopy();
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        #endregion

    }   
    /// <summary>
    /// 会议属性
    /// </summary>
    public class AppointmentProperty
    {
        /// <summary>
        /// 会议ID
        /// </summary>
        public ItemId ID { get; set; }
        /// <summary>
        /// 会议标题
        /// </summary>
        public string Subject { get; set; }
        /// <summary>
        /// 会议内容
        /// </summary>
        public string Body { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime Start { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime End { get; set; }
        /// <summary>
        /// 会议地点
        /// </summary>
        public string Location { get; set; }
        /// <summary>
        /// 与会人员（含会议室邮箱）
        /// </summary>
        public AttendeeCollection Attendees { get; set; }
    }
}