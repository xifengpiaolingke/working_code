﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="EWSMeetingRoom.Index" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
<meta http-equiv="refresh" content="5"/>
<link rel="Stylesheet" type="text/css" media="all" href="Styles/print.css" />
<link rel="Stylesheet" type="text/css" media="all" href="Styles/style.css" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label ID="labTime" runat="server" Text=""></asp:Label>
        <asp:Table ID="Table1" runat="server" BorderWidth=1px Width="100%" BorderStyle="Solid" CellPadding="10" CellSpacing="0"  >
            <asp:TableRow ID="header" runat="server" Width="20%" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" Font-Bold="True">
                <asp:TableCell runat="server" BorderStyle="Solid" BorderWidth="1px">Meeting Room 102</asp:TableCell>
                <asp:TableCell runat="server" BorderStyle="Solid" BorderWidth="1px">Meeting Room 118</asp:TableCell>
                <asp:TableCell ID="TableCell1" runat="server" BorderStyle="Solid" BorderWidth="1px">Meeting Room 119</asp:TableCell>
                <asp:TableCell ID="TableCell2" runat="server" BorderStyle="Solid" BorderWidth="1px">Meeting Room 120</asp:TableCell>
                <asp:TableCell ID="TableCell3" runat="server" BorderStyle="Solid" BorderWidth="1px">Meeting Room 121</asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="TableRow1" runat="server"  BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px">
                <asp:TableCell ID="r1Room102" runat="server" BorderStyle="Solid" BorderWidth="1px"></asp:TableCell>
                <asp:TableCell ID="r1Room118" runat="server" BorderStyle="Solid" BorderWidth="1px"></asp:TableCell>
                <asp:TableCell ID="r1Room119" runat="server" BorderStyle="Solid" BorderWidth="1px"></asp:TableCell>
                <asp:TableCell ID="r1Room120" runat="server" BorderStyle="Solid" BorderWidth="1px"></asp:TableCell>
                <asp:TableCell ID="r1Room121" runat="server" BorderStyle="Solid" BorderWidth="1px"></asp:TableCell>
            </asp:TableRow>
             <asp:TableRow ID="TableRow2" runat="server"  BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px">
                <asp:TableCell ID="r2Room102" runat="server" BorderStyle="Solid" BorderWidth="1px"></asp:TableCell>
                <asp:TableCell ID="r2Room118" runat="server" BorderStyle="Solid" BorderWidth="1px"></asp:TableCell>
                <asp:TableCell ID="r2Room119" runat="server" BorderStyle="Solid" BorderWidth="1px"></asp:TableCell>
                <asp:TableCell ID="r2Room120" runat="server" BorderStyle="Solid" BorderWidth="1px"></asp:TableCell>
                <asp:TableCell ID="r2Room121" runat="server" BorderStyle="Solid" BorderWidth="1px"></asp:TableCell>
            </asp:TableRow>
             <asp:TableRow ID="TableRow3" runat="server"  BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px">
                <asp:TableCell ID="r3Room102" runat="server" BorderStyle="Solid" BorderWidth="1px"></asp:TableCell>
                <asp:TableCell ID="r3Room118" runat="server" BorderStyle="Solid" BorderWidth="1px"></asp:TableCell>
                <asp:TableCell ID="r3Room119" runat="server" BorderStyle="Solid" BorderWidth="1px"></asp:TableCell>
                <asp:TableCell ID="r3Room120" runat="server" BorderStyle="Solid" BorderWidth="1px"></asp:TableCell>
                <asp:TableCell ID="r3Room121" runat="server" BorderStyle="Solid" BorderWidth="1px"></asp:TableCell>
            </asp:TableRow>
             <asp:TableRow ID="TableRow4" runat="server"  BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px">
                <asp:TableCell ID="r4Room102" runat="server" BorderStyle="Solid" BorderWidth="1px"></asp:TableCell>
                <asp:TableCell ID="r4Room118" runat="server" BorderStyle="Solid" BorderWidth="1px"></asp:TableCell>
                <asp:TableCell ID="r4Room119" runat="server" BorderStyle="Solid" BorderWidth="1px"></asp:TableCell>
                <asp:TableCell ID="r4Room120" runat="server" BorderStyle="Solid" BorderWidth="1px"></asp:TableCell>
                <asp:TableCell ID="r4Room121" runat="server" BorderStyle="Solid" BorderWidth="1px"></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="TableRow5" runat="server"  BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px">
                <asp:TableCell ID="r5Room102" runat="server" BorderStyle="Solid" BorderWidth="1px"></asp:TableCell>
                <asp:TableCell ID="r5Room118" runat="server" BorderStyle="Solid" BorderWidth="1px"></asp:TableCell>
                <asp:TableCell ID="r5Room119" runat="server" BorderStyle="Solid" BorderWidth="1px"></asp:TableCell>
                <asp:TableCell ID="r5Room120" runat="server" BorderStyle="Solid" BorderWidth="1px"></asp:TableCell>
                <asp:TableCell ID="r5Room121" runat="server" BorderStyle="Solid" BorderWidth="1px"></asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>
    </form>
</body>
</html>
