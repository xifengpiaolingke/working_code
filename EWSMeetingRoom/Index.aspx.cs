﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Exchange.WebServices;
using Microsoft.Exchange.WebServices.Data;
using Microsoft.Exchange.WebServices.Autodiscover;
using System.Configuration;
using System.IO;
using System.Text;
using System.Net;


namespace EWSMeetingRoom
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            labTime.Text = DateTime.Now.ToString("r");
            string[] room = new string[] { "102", "118","119","120","121" };
            try
            {
                DateTime now = Convert.ToDateTime(DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString()+"-"+DateTime.Now.Day.ToString()); //DateTime.Now;
                //DateTime now = Convert.ToDateTime("2017-8-22"); //DateTime.Now;
                DateTime startDate = now;
                DateTime endDate = now.AddDays(1);

                for (int j = 0; j < 5; j++)
                {
                    List<Appointment> appointments = GetMeetings(startDate, endDate, "bsce_meetingroom_"+ room[j]+".cn@siemens.com");
                    int i = 1;                    
                    foreach (Appointment a in appointments)
                    {
                        string v = "";
                        v = v + a.Start.ToString().Substring(a.Start.ToString().IndexOf(" ")) + " - " + a.End.ToString().Substring(a.End.ToString().IndexOf(" "));
                        if (a.Subject != null)
                        {
                            if (a.Subject.ToString().IndexOf("(") > 0)
                                v = v + "<br>" + a.Subject.ToString().Substring(0, a.Subject.ToString().IndexOf("(") - 1);
                            if (a.Subject.ToString().IndexOf(")") > 0)
                                v = v + "<br>" + a.Subject.ToString().Substring(a.Subject.ToString().IndexOf(")") + 1);
                        }  
                            //v = v + "<br>" + a.Subject.ToString().Substring(0,a.Subject.ToString().IndexOf("(")-1) + "<br>" + a.Subject.ToString().Substring(a.Subject.ToString().IndexOf(")")+1);
                        TableCell p = this.FindControl("r" + i.ToString() + "Room" + room[j]) as TableCell;
                        p.Text = v;
                        i = i + 1;
                    }
                }
            }
            catch (Exception ee)
            {
            }
        }

        /// <summary>
        /// 获取用户在某段时间内的所有会议。最长时间间隔为42天。
        /// </summary>
        /// <param name="startDate">要查询的开始时间。</param>
        /// <param name="endDate">要查询的结束时间</param>
        /// <param name="mailbox">要查询的邮箱日历，如果查询当前登录的用户的日历，可不填。</param>
        /// <returns>返回一个会议列表。列表对象不包含详细信息（如接受拒绝状态等），需使用<see cref="GetMeetingStatus"/> 方法获取。</returns>
        public List<Appointment> GetMeetings(DateTime startDate, DateTime endDate, string mailbox = null)
        {
            ExchangeService Service = GetExchangeService();
            CalendarFolder calendar = null;

            if (mailbox == null)
            {
                calendar = CalendarFolder.Bind(Service, WellKnownFolderName.Calendar);
            }
            else
            {
                FolderId folderid = new FolderId(WellKnownFolderName.Calendar, mailbox);
                calendar = CalendarFolder.Bind(Service, folderid);
            }

            // Set the start and end time and number of appointments to retrieve.
            CalendarView cView = new CalendarView(startDate, endDate);

            // Limit the properties returned to the appointment's subject, start time, and end time.
            cView.PropertySet = new PropertySet(AppointmentSchema.Subject,
                AppointmentSchema.LastModifiedTime,
                AppointmentSchema.IsMeeting,
                AppointmentSchema.Start,
                AppointmentSchema.End);

            // Retrieve a collection of appointments by using the calendar view.
            FindItemsResults<Appointment> appointments = calendar.FindAppointments(cView);
            return appointments.Where(m => m.IsMeeting).ToList();
        }
        private ExchangeService GetExchangeService()
        {
            ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2013);            
            service.Credentials = new NetworkCredential(@"ad001\z003wkdd", "5tgb^YHN");
            service.Url = new Uri("https://cnpek01905.cn001.siemens.net/EWS/Exchange.asmx");
            return service;
        }
    }
}