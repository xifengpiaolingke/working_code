﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Exchange.WebServices;
using Microsoft.Exchange.WebServices.Data;
using Microsoft.Exchange.WebServices.Autodiscover;
using System.Configuration;
using System.IO;
using System.Text;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace EWSMeetingRoom
{
    public partial class index1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string[] room = new string[] { "118","119","120" };

            try
            {
                DateTime now = Convert.ToDateTime(DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString()); //DateTime.Now;
                DateTime startDate = now;
                DateTime endDate = now.AddDays(1);

                for (int j = 0; j < 5; j++)
                {
                    //List<Appointment> appointments = GetMeetings(startDate, endDate, "");
                    List<Appointment> appointments = GetMeetings(startDate, endDate, "bsce_meetingroom_" + room[j] + ".cn@siemens.com");

                    foreach (Appointment a in appointments)
                    {
                        string subject="";                                                
                        string user="";
                        
                        string date="";
                        switch (a.Start.Month)
                        {
                            case 1:
                                date = "Jan.";
                                break;
                            case 2:
                                date="Feb.";
                                break;
                            case 3:
                                date = "Mar.";
                                break;
                            case 4:
                                date = "Apr.";
                                break;
                            case 5:
                                date = "May.";
                                break;
                            case 6:
                                date = "Jun.";
                                break;
                            case 7:
                                date = "Jul.";
                                break;
                            case 8:
                                date = "Aug.";
                                break;
                            case 9:
                                date = "Sep.";
                                break;
                            case 10:
                                date = "Oct.";
                                break;
                            case 11:
                                date = "Nov.";
                                break;
                            case 12:
                                date = "Dec.";
                                break;
                            default:
                                date="";
                                break;
                        }
                        date = date + " " + a.Start.Day.ToString();
                        if (a.Start.Day != a.End.Day)
                            date = date + "-" + a.End.Day.ToString();
                        //string time = a.Start.ToString("H")+ " - " + a.End.ToString("H");
                        string time = a.Start.ToString("yyyy-MM-dd HH:mm").Substring(a.Start.ToString().IndexOf(" ")+1) + " - " + a.End.ToString("yyyy-MM-dd HH:mm").Substring(a.End.ToString().IndexOf(" ")+1);
                        if (a.Subject != null)
                        {
                            if (a.Subject.ToString().IndexOf("(") > 0)
                                user =  a.Subject.ToString().Substring(0, a.Subject.ToString().IndexOf("(") -1);
                            else
                                user = a.Subject.ToString();
                            if (a.Subject.ToString().IndexOf(")") > 0)
                                subject =  a.Subject.ToString().Substring(a.Subject.ToString().IndexOf(")") + 1);
                            else
                                subject = a.Subject.ToString();
                            if (subject.Length<=1)
                                subject = user + " Meeting";
                        }
                        //v = v + "<br>" + a.Subject.ToString().Substring(0,a.Subject.ToString().IndexOf("(")-1) + "<br>" + a.Subject.ToString().Substring(a.Subject.ToString().IndexOf(")")+1);
                        //TableCell p = this.FindControl("r" + i.ToString() + "Room" + room[j]) as TableCell;
                        //p.Text = v;
                        TableRow row = new TableRow();
                        row.Height = 38;
                        TableCell cellSubject = new TableCell();
                        cellSubject.Text= subject;
                        row.Cells.Add(cellSubject);

                        TableCell cellDate = new TableCell();
                        cellDate.Text = "&nbsp;&nbsp;" + date;
                        row.Cells.Add(cellDate);

                        TableCell cellTime = new TableCell();
                        cellTime.Text = time;
                        row.Cells.Add(cellTime);

                        TableCell cellUser = new TableCell();
                        cellUser.Text = user;
                        row.Cells.Add(cellUser);

                        TableCell cellRoom = new TableCell();
                        cellRoom.Text = room[j];
                        row.Cells.Add(cellRoom);

                        tbShow.Rows.Add(row);
                       
                    }
                }
            }
            catch (Exception ee)
            {
            }
        }

        /// <summary>
        /// 获取用户在某段时间内的所有会议。最长时间间隔为42天。
        /// </summary>
        /// <param name="startDate">要查询的开始时间。</param>
        /// <param name="endDate">要查询的结束时间</param>
        /// <param name="mailbox">要查询的邮箱日历，如果查询当前登录的用户的日历，可不填。</param>
        /// <returns>返回一个会议列表。列表对象不包含详细信息（如接受拒绝状态等），需使用<see cref="GetMeetingStatus"/> 方法获取。</returns>
        public List<Appointment> GetMeetings(DateTime startDate, DateTime endDate, string mailbox = null)
        {
            ExchangeService Service = GetExchangeService();
            CalendarFolder calendar = null;

            if (mailbox == null)
            {
                calendar = CalendarFolder.Bind(Service, WellKnownFolderName.Calendar);
            }
            else
            {
                //Folder inbox = Folder.Bind(Service, WellKnownFolderName.Inbox);//Inbox文件夹，不包括子文件夹
                FolderId folderid = new FolderId(WellKnownFolderName.Calendar, mailbox);
                calendar = CalendarFolder.Bind(Service, folderid);
            }

            // Set the start and end time and number of appointments to retrieve.
            CalendarView cView = new CalendarView(startDate, endDate);

            // Limit the properties returned to the appointment's subject, start time, and end time.
            cView.PropertySet = new PropertySet(AppointmentSchema.Subject,
                AppointmentSchema.LastModifiedTime,
                AppointmentSchema.IsMeeting,
                AppointmentSchema.Start,
                AppointmentSchema.End,
                AppointmentSchema.MyResponseType
                );

            // Retrieve a collection of appointments by using the calendar view.
            FindItemsResults<Appointment> appointments = calendar.FindAppointments(cView);
            return appointments.Where(m => m.IsMeeting && m.MyResponseType == MeetingResponseType.Accept).ToList();
        }
        private ExchangeService GetExchangeService()
        {
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);
            MyConfig config = (MyConfig)ConfigurationManager.GetSection("myConfig");
            ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2013);
            //service.Credentials = new NetworkCredential(@"ad001\z0039efr", "Fxc1478963");
            service.Credentials = new WebCredentials(new NetworkCredential(config.Key, config.Value,""));
            service.PreAuthenticate=true;
          //  service.Credentials = new NetworkCredential(@"cn001\cn1bm011", "2wsx#EDC");
            service.Url = new Uri("https://mail-cn.siemens.net/EWS/exchange.asmx");
            return service;
        }
        public bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
        {
            return true;
        }

    }
}